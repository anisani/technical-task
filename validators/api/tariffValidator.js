const Validator = require("validator");
const isEmpty = require("../isEmpty");
const mongoose = require("mongoose");

const NetworkOperator = mongoose.model("networkOperators");
const Tariff = mongoose.model("tariffs");

exports.validateTariffInputs = async (data) => {
  const errors = [];
  const networkCodes = [];
  if (data.tariffs.length) {
    for (const tariff of data.tariffs) {
      if (!Validator.isLength(tariff.zone, { min: 1 })) {
        errors.push("Zone should not be empty");
      }
      if (!Validator.isLength(tariff.country, { min: 1 })) {
        errors.push("Country should not be empty");
      }
      if (!Validator.isLength(tariff.networkOperator, { min: 1 })) {
        errors.push("Network operator should not be empty");
      }
      if (
        typeof tariff.networkCode !== "undefined" &&
        !Validator.isNumeric(tariff.networkCode.toString())
      ) {
        errors.push("Network code should be number");
      }
      if (networkCodes.includes(tariff.networkCode)) {
        errors.push("Network code should be unique");
      }
      networkCodes.push(tariff.networkCode);
      if (
        typeof tariff.increment !== "undefined" &&
        !["KB", "MB"].includes(tariff.increment)
      ) {
        errors.push(
          "Increment is required and should be any value 'KB' / 'MB'"
        );
      }

      // Check for unexpected keys in zone object
      const unexpectedKeys = Object.keys(tariff).filter(
        (key) =>
          ![
            "zone",
            "country",
            "networkOperator",
            "networkCode",
            "increment",
          ].includes(key)
      );
      if (unexpectedKeys.length) {
        errors.push(
          `Unexpected key(s) found in tariff details : ${unexpectedKeys.join(
            ", "
          )}`
        );
      }

      // check zone and network operator exists in table
      if (
        typeof tariff.networkOperator !== "undefined" &&
        typeof tariff.zone !== "undefined"
      ) {
        const operator = await NetworkOperator.findOne({
          operator: tariff.networkOperator,
          zones: { $elemMatch: { name: tariff.zone } },
        });
        if (!operator) {
          errors.push(
            `Mismatch in network operators and zone: (networkOperator: ${tariff.networkOperator}, zone: ${tariff.zone})`
          );
        }
      }

      // Check network code already exists
      if (networkCodes.length) {
        if(await Tariff.findOne({
          networkCode: { $in: networkCodes },
        })) {
          errors.push("Network codes already exists")
        }
      }
    }
  } else {
    errors.push("Tariff should not be empty");
  }

  return {
    errors: [...new Set(errors)],
    isValid: isEmpty(errors),
  };
};
