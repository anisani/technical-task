const Validator = require("validator");
const isEmpty = require("../isEmpty");

exports.validateNetworkOperatorInputs = (data) => {
  const errors = [];
  if (typeof data.networkOperator === "undefined") data.networkOperator = "";

  if (Validator.isEmpty(data.networkOperator)) {
    errors.push("Network operator is required");
  }

  // zoneNames for checking name unique
  const zoneNames = [];

  if (data.zoneDetails.length) {
    for (const zone of data.zoneDetails) {
      if (zone.name) {
        if (!Validator.isLength(zone.name, { min: 1 })) {
          errors.push("Zone name is required for all zones");
        }
        if (zoneNames.includes(zone.name)) {
          errors.push("Zone name should be unique");
        }

        zoneNames.push(zone.name);
      } else {
        errors.push("Zone name should not be empty")
      }

      if (typeof zone.price !== 'undefined') {
        if (!Validator.isNumeric(zone.price.toString())) {
          errors.push("Price should be number for all zones");
        }
  
        if (zone.price <= 0) {
          errors.push("Price should be greater than 0 for all zones");
        }
      } else {
        errors.push("Zone price should not be empty")
      }

      // Check for unexpected keys in zone object
      const unexpectedKeys = Object.keys(zone).filter(
        (key) => key !== "name" && key !== "price"
      );
      if (unexpectedKeys.length) {
        errors.push(
          `Unexpected key(s) fond in zone details : ${unexpectedKeys.join(
            ", "
          )}`
        );
      }
    }
  } else {
    errors.push("Zone details should not be empty");
  }

  return {
    errors: [...new Set(errors)],
    isValid: isEmpty(errors),
  };
};
