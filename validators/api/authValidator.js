const Validator = require("validator");
const isEmpty = require("../isEmpty");

exports.validateSignUpInputs = (data) => {
  const errors = [];
  if (typeof data.email === "undefined") data.email = "";
  if (typeof data.password === "undefined") data.password = "";

  if (Validator.isEmpty(data.email)) {
    errors.push("Email ID is required");
  }

  if (!Validator.isEmail(data.email)) {
    errors.push('Invalid email ID')
  }

  if (Validator.isEmpty(data.password)) {
    errors.push("Password is required");
  }

  return {
    errors,
    isValid: isEmpty(errors),
  };
};
