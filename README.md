# Technical Task
REST API Application using Nodejs

## Prerequisites

Before you can run the project, make sure you have Node.js and npm (Node Package Manager) installed on your machine.



## Installation

To set up and install the project, follow these steps:

1. Clone this repository to your local machine.
git clone https://gitlab.com/anisani/technical-task.git```

2. Install the project dependencies using npm
npm install

3. Create a .env file in the root folder

4.Copy values from .env-example and paste to created .env file.

## Usage
*npm run start*  to run the project