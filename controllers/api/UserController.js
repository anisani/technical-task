const { response } = require("../../utils/responses");
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const { validateSignUpInputs } = require("../../validators/api/authValidator");

const User = mongoose.model("users");

exports.isEmailExists = async (email) => {
  return await User.countDocuments({ email });
};

exports.signUp = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Validation checks
    const { errors, isValid } = validateSignUpInputs(req.body);
    if (!isValid) {
      return res.status(400).json(response(400, "Bad request", errors));
    }

    // Check if email is already exists
    if (await this.isEmailExists(email)) {
      return res
        .status(409)
        .json(response(409, "Email already exists", { email }));
    }

    // Generate salt and hash for password encryption
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);

    // Create new user
    const user = new User({
      email,
      hash,
    });

    await user.save();

    return res.status(201).json(
      response(201, "SignUp successfully", {
        _id: user._id,
        email: user.email,
      })
    );
  } catch (err) {
    return res.status(400).json(response(400, "Filed to signup", {}));
  }
};

exports.signIn = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Validation checks
    const { errors, isValid } = validateSignUpInputs(req.body);
    if (!isValid) {
      return res.status(400).json(response(400, "Bad request", errors));
    }

    // Check user exists
    const user = await User.findOne({ email });
    if (user && await user.validatePassword(password, user.hash)) {
      const jwtPrivateKey = process.env.JWT_PRIVATE_KEY;
      const token = jwt.sign(
        {
          _id: user._id,
          email: user.email,
          createdAt: user.createdAt,
        },
        jwtPrivateKey
      );

      return res.status(200).json(response(200, "Login successfully", {token}))
    } else {
      return res.status(401).json(response(401, "Invalid login credentials"));
    }
  } catch (err) {
    return res.status(400).json(response(400, "Filed to sign-in", {}));
  }
};
