const mongoose = require("mongoose");
const { response } = require("../../utils/responses");
const { validateTariffInputs } = require("../../validators/api/tariffValidator");

const Tariff = mongoose.model("tariffs");

exports.create = async (req, res) => {
  try {
    const { tariffs } = req.body;

    // Validation checks
    const { errors, isValid } = await validateTariffInputs(req.body);
    if (!isValid) {
      return res.status(400).json(response(400, "Bad request", errors));
    }

    // Save Tariffs
    await Tariff.insertMany(tariffs);
    return res.status(201).json(response(201, 'Tariffs saved successfully', tariffs))
  } catch (err) {
    console.log(err)
    return res.status(400).json(response(400, "Failed to save tariff"));
  }
};
