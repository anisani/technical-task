const mongoose = require("mongoose");
const { response } = require("../../utils/responses");
const {
  validateNetworkOperatorInputs,
} = require("../../validators/api/networkValidator");

const NetworkOperator = mongoose.model("networkOperators");

exports.isNetworkOperatorExists = async (operator) => {
  const regex = { $regex: new RegExp(`.*${operator.trim()}.*`, "i") };
  return await NetworkOperator.countDocuments({ operator: regex });
};

exports.create = async (req, res) => {
  try {
    const { networkOperator, zoneDetails } = req.body;

    // Validation checks
    const { errors, isValid } = validateNetworkOperatorInputs(req.body);
    if (!isValid) {
      return res.status(400).json(response(400, "Bad request", errors));
    }

    // Check network operator already exists
    if (await this.isNetworkOperatorExists(networkOperator)) {
      return res
        .status(409)
        .json(
          response(409, "Network operator name already exists", networkOperator)
        );
    }

    const operator = new NetworkOperator({
      operator: networkOperator,
      zones: zoneDetails,
    });
    await operator.save();
    return res
      .status(201)
      .json(response(201, "Network operator added successfully", operator));
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json(response(400, "Failed to create network operator", {}));
  }
};

exports.findAll = async (req, res) => {
  try {
    // const networkOperators = await NetworkOperator.find({}, {__v: 0})

    const networkOperators = await NetworkOperator.aggregate([
      {
        $project: {
          operator: 1,
          zones: {
            $map: {
              input: "$zones",
              as: "zone",
              in: {
                name: "$$zone.name",
                price: {
                  $round: [{ $divide: ["$$zone.price", 100] }, 2],
                },
              },
            },
          },
        },
      },
    ]);
    return res
      .status(200)
      .json(response(200, "All network operators list", networkOperators));
  } catch (err) {
    return res
      .status(400)
      .json(response(400, "Failed to fetch network operators"));
  }
};
