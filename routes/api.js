var express = require("express");
var router = express.Router();

const { signIn, signUp } = require("../controllers/api/UserController");
const networkOperatorController = require("../controllers/api/NetworkOperatorController");
const tariffController = require('../controllers/api/TariffController')
const isAuth = require("../middlewares/isAuth");

/* GET users listing. */
router.get("/", function (req, res, next) {
  return res.json({ status: true });
});

// Auth modules
router.post("/v1/signup", signUp);
router.post("/v1/signin", signIn);

// Dashboard modules
router.post("/v1/networkOperators", isAuth, networkOperatorController.create);
router.get("/v1/networkOperators", isAuth, networkOperatorController.findAll);

router.post("/v1/tariff", isAuth, tariffController.create)

module.exports = router;
