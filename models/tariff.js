const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TariffSchema = new Schema({
  zone: { type: String },
  country: { type: String },
  networkOperator: { type: String },
  networkCode: { type: Number },
  increment: { type: String, enum: ["KB", "MB"] },
});

module.exports = tariffs = mongoose.model("tariffs", TariffSchema);
