const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

const UsersSchema = new Schema({
  name: { type: String, trim: true },
  email: { type: String, trim: true },
  hash: { type: String },
  createdAt: { type: Date, default: Date.now() },
});

UsersSchema.methods.validatePassword = async (password, hash) => {
  return await bcrypt.compare(password, hash).then((res) => res);
};

module.exports = users = mongoose.model("users", UsersSchema);
