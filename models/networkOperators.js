const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const NetworkOperatorsSchema = new Schema({
  operator: { type: String, trim: true },
  zones: [
    {
      name: { type: String, trim: true },
      price: {
        type: Number,
        min: 0.01,
        get: (v) => (v / 100).toFixed(2),
        set: (v) => Math.round(v * 100),
      },
    },
  ],
});

module.exports = networkOperators = mongoose.model('networkOperators', NetworkOperatorsSchema)
