const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const { response } = require("../utils/responses");

const User = mongoose.model("users");

module.exports = async (req, res, next) => {
  const token = req.header("x-auth-token");
  if (!token) {
    return res
      .status(401)
      .json(response(401, "Access denied. No token provided"));
  }

  const jwtPrivateKey = process.env.JWT_PRIVATE_KEY;
  try {
    const decode = jwt.verify(token, jwtPrivateKey);

    if (decode) {
      const user = await User.findOne({ _id: decode._id });
      if (user) {
        next();
      } else {
        return res.status(401).json(response(401, "Invalid token"));
      }
    } else {
      return res.status(401).json(response(401, "Invalid token"));
    }
  } catch (err) {
    return res.status(401).json(response(401, "Invalid token"));
  }
};
